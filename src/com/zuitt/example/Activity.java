package com.zuitt.example;
import java.util.*;
public class Activity {
    public static void main(String[] args) {
        HashMap<String, Integer> products = new HashMap<>();
        products.put("Mario Oddysey", 50);
        products.put("Super Smash Bros. Ultimate", 20);
        products.put("Luigi's Mansion", 15);
        products.put("Pokemon Sword", 30);
        products.put("Pokemon Shield", 100);
        products.forEach((key, value) -> {
            System.out.println(key + "has " + value + " stocks left ");
        });
        System.out.println("");
        ArrayList<String> topGames = new ArrayList<>();
        products.forEach((key, value) -> {
            if (value <= 30) {
                topGames.add(key);
                System.out.println(key + " has been added to top games list.");
            }
        });
        System.out.println("Our shop's top games: " + topGames);
        System.out.println("");
        Scanner userInput = new Scanner(System.in);
        int x = 0;
        do {
            System.out.println("Would you like to add an item (yes/no):");
            String addItem = userInput.nextLine();
            if(Objects.equals(addItem, "yes")){
                System.out.println("Item Name:");
                String itemName = userInput.nextLine();
                System.out.println("Item Stocks:");
                Integer itemStocks = Integer.valueOf(userInput.nextLine());
                products.put(itemName,itemStocks);
                System.out.println(products);
            }
            else if(Objects.equals(addItem, "no")){
                System.out.println("Thank you.");
                x=1;
            }
            else{
                System.out.println("Invalid input. Please try again.");
            }
        } while (x == 0);
    }
}
